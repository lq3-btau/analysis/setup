#!/bin/sh

git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/CxAODTools.git
git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/CxAODTools_HH_bbtautau.git
git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/CxAODReader.git
git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/CxAODReader_HH_bbtautau.git
git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/CxAODOperations.git
git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/MIA.git
git clone ssh://git@gitlab.cern.ch:7999/lq3-btau/analysis/ParametricNet.git

# external packages
## These packages aren't updaed by myself
git clone ssh://git@gitlab.cern.ch:7999/MIA/HbbMVA.git
git clone ssh://git@gitlab.cern.ch:7999/CxAODFramework/CxAODMaker.git
git clone ssh://git@gitlab.cern.ch:7999/CxAODFramework/CorrsAndSysts.git
git clone ssh://git@gitlab.cern.ch:7999/CxAODFramework/CxAODTools_VHbb
git clone git@github.com:lwtnn/lwtnn.git

#
mkdir build
cp setup/CMakeList.txt .
cd build 
